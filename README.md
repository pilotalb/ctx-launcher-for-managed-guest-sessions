# CTX Launcher for Managed Guest Sessions

A helper extension for Chrome Enterprise customers with requirements to auto-launch Citrix Workspace after entering a managed guest session

Availble in the Chrome Web Store: https://chrome.google.com/webstore/detail/jlohdpkkidomobhljjloddbjhiaphihe